//
//  HelpViewController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/12/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    @IBOutlet weak var sendBt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sendBt.layer.cornerRadius = 35
    }
    

}
