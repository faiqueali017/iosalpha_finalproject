//
//  ChecklistItem.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/9/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import Foundation

class ChecklistItem{
    var text = ""
    var checked = false
    
    func toggleChecked(){
        checked = !checked
    }
}
