//
//  SignUpController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 9/19/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {

    @IBOutlet weak var signUPbt: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confrimPassword: UITextField!
    @IBOutlet weak var email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        signUPbt.layer.cornerRadius = 35
    }
    
    
    
    @IBAction func signUpbtPressed(_ sender: Any) {
        var usernameTxt: String
        usernameTxt = username.text!
        
        var Password: String
        Password = password.text!
        
        var Confpassword: String
        Confpassword = confrimPassword.text!
        
        var Email: String
        Email = email.text!
        
        if(usernameTxt.isEmpty && Password.isEmpty && Confpassword.isEmpty && Email.isEmpty){
            let alert = UIAlertController(title: "ALERT", message: "FIELDS EMPTY!", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    
        
    }
    @IBAction func done(_ sender: UIBarButtonItem) {
    
        dismiss(animated: true, completion: nil)
    }
    
    

}
