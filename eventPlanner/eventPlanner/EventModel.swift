//
//  EventModel.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/11/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class EventModel: NSObject {
    var title: String = ""
    var destination: String = ""
    var date: String = ""
    var time: String = ""
    var persons: String = ""
    
    func eventName() -> String{
        return title
    }
    
    func eventDestination() -> String{
        return destination
    }
    
    func eventDate() -> String{
        return date
    }
    
    func eventTime() -> String{
        return time
    }
    
    func eventPersons() -> String{
        return persons
    }

}
