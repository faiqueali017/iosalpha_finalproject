//
//  ViewController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 9/18/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //signInButton.backgroundColor = UIColor(red: 204, green: 153, blue: 0, alpha: 100)
        signInButton.layer.cornerRadius = 35
        
    }

    
    @IBAction func loginButton(_ sender: UIButton) {
        var myUsername: String
        myUsername = username.text!
        
        var myPassword: String
        myPassword = password.text!
        
        if(myUsername == "admin" && myPassword == "admin"){
            performSegue(withIdentifier: "homeSegue", sender: nil)
            print("Succesfull")
            
        }else if (myUsername.isEmpty && myPassword.isEmpty ){
            let alert = UIAlertController(title: "ALERT", message: "FIELDS EMPTY!", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "OOPS", message: "DON'T HAVE AN ACCOUNT", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

