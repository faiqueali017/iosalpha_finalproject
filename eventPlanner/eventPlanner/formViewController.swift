//
//  formViewController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/11/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class formViewController: UIViewController {

    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var destinationName: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var noOfPersons: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var myEvent = EventModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker?.datePickerMode = .dateAndTime
        
        datePicker?.minimumDate = Date.calculateDate(day: 1, month: 1, year: 2012, hour: 0, min: 0)
        datePicker?.maximumDate = Date.calculateDate(day: 31, month: 1, year: 2022, hour: 0, min: 0)
        
        
        let tbc = tabBarController as! tabBarViewController
        myEvent = tbc.myEvent
    }
    
    
    @IBAction func PersonStepper(_ sender: UIStepper) {
        var value: Int
        value = Int(sender.value)
        noOfPersons.text = String(value)
    }
    
    @IBAction func valueChanges(sender: UIDatePicker, forEvent event: UIEvent){
        dateLabel?.text = "\(sender.date.getDMYHM().day)-\(sender.date.getDMYHM().month)-\(sender.date.getDMYHM().year)"
        
        timeLabel?.text = "\(sender.date.getDMYHM().hour):\(sender.date.getDMYHM().min):\(sender.date.getDMYHM().second)"
    }
  
    
    @IBAction func done(_ sender: Any) {
        myEvent.title = eventName.text!
        myEvent.destination = destinationName.text!
        myEvent.date = dateLabel.text!
        myEvent.time = timeLabel.text!
        myEvent.persons = noOfPersons.text!
        
        let alert = UIAlertController(title: "ALERT", message: "Successful!", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
