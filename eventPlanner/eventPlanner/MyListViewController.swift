//
//  MyListViewController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/11/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class MyListViewController: UIViewController {

    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var destinationName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var persons: UILabel!
    
    var myEvent = EventModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myvc = tabBarController as! tabBarViewController
        myEvent = myvc.myEvent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        eventName.text = myEvent.eventName()
        destinationName.text = myEvent.eventDestination()
        dateLabel.text = myEvent.eventDate()
        timeLabel.text = myEvent.eventTime()
        persons.text = myEvent.eventPersons()
    }

}
