//
//  CheckistViewController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 10/9/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class CheckistViewController: UITableViewController {
    
    var row0item: ChecklistItem
    var row1item: ChecklistItem
    
    var items: [ChecklistItem]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        items = [ChecklistItem]()
        
        row0item = ChecklistItem()
        row0item.text = "Walk"
        row0item.checked = false
        items.append(row0item)
        
        row1item = ChecklistItem()
        row1item.text = "dog"
        row1item.checked = true
        items.append(row1item)
        
        super.init(coder: aDecoder)
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        
        let item = items[indexPath.row]
        
        configureText(for: cell, with: item)
        
        configureCheckmark(for: cell, at: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath){
            let item = items[indexPath.row]
            item.toggleChecked()
            configureCheckmark(for: cell, at: indexPath)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        items.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    func configureCheckmark(for cell: UITableViewCell, at indexPath: IndexPath){
        let item = items[indexPath.row]
        
        if item.checked{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        
    }
    
    
    func configureText(for cell: UITableViewCell, with item: ChecklistItem){
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    
}
