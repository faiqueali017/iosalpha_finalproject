//
//  HomeController.swift
//  eventPlanner
//
//  Created by Faiq Ali on 9/18/19.
//  Copyright © 2019 Faiq Ali. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    @IBOutlet weak var picnicView: UIView!
    @IBOutlet weak var picnicImageView: UIImageView!
    
    @IBOutlet weak var concertImageView: UIImageView!
    @IBOutlet weak var concertView: UIView!
    
    @IBOutlet weak var meetingsImageView: UIImageView!
    @IBOutlet weak var meetingsView: UIView!
    
    @IBOutlet weak var seminarImageView: UIImageView!
    @IBOutlet weak var seminarView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        picnicView.layer.cornerRadius = 10
        picnicImageView.layer.cornerRadius = 10
        
        concertView.layer.cornerRadius = 10
        concertImageView.layer.cornerRadius = 10
        
        meetingsView.layer.cornerRadius = 10
        meetingsImageView.layer.cornerRadius = 10
        
        seminarView.layer.cornerRadius = 10
        seminarImageView.layer.cornerRadius = 10
    }
    

    

}
